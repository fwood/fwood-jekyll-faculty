---
title: People
layout: default
permalink: /people/
---

### Postdocs
- [Alexander Mead](https://scholar.google.com/citations?user=ps6MgnAAAAAJ&hl=en)

### UBC PhD students
- [Xiaoxuan Liang](https://www.linkedin.com/in/xiaoxuan-liang-4451a4171/?originalSubdomain=ca)
- [Matt Niedoba](https://www.linkedin.com/in/mniedoba/?originalSubdomain=ca)
- [Larry Liu](https://www.linkedin.com/in/larry-liu-323b51126/?originalSubdomain=ca)
- [Christian Weilbach](https://scholar.google.com/citations?user=6foQfZwAAAAJ&hl=en)
- [Will Harvey](https://www.cs.ubc.ca/~wsgh/)
- [Vaden Masrani](https://vmasrani.github.io/)
- [Andreas Munk](https://ammunk.com/home/index.php)
- [Wilder Lavington](https://wilderlavington.github.io/)
- [Saeid Naderiparizi](https://www.cs.ubc.ca/~saeidnp/)
- [Jason Yoo](https://www.linkedin.com/in/jjsyoo/)
- [Ryan Fayyazi](https://rfayyazi.github.io)
- [Vasileios Lioutas](https://www.vlioutas.com/)

### UBC Masters Students

- [Dylan Green](https://www.linkedin.com/in/dylangreen90/?originalSubdomain=ca)


<!--
### UBC Undergraduate Mentees

- [Ray Ding](https://www.linkedin.com/in/raydz/)
- [Olga Solodova](https://www.linkedin.com/in/olga-solodova-9b138318b/)
- [Zikun Chen]()
- [Alexander Bergholm](https://www.linkedin.com/in/alexander-bergholm/?originalSubdomain=ca)
- [Elizabeth Hnatiuk](https://www.linkedin.com/in/elizabeth-hnatiuk-871558162/)
-->

<!-- ### MS students -->
### Alumni
- [Michael Teng](https://www.linkedin.com/in/michael-teng-50555664/) -> Hyundai
- [Adam Scibior](http://mlg.eng.cam.ac.uk/adam/) -> Inverted AI CTO
- [Berend Zwartsenberg](https://bzwartsenberg.github.io/) -> Inverted AI CSO
- [Peyman Bateni](https://www.cs.ubc.ca/~pbateni/) -> founder Beam AI
- [Andrew Warrington](http://www.robots.ox.ac.uk/~andreww) -> Stanford PostDoc
- [Adam Golinski](http://adamgol.me/) -> Oxford PhD
- [Bradley Gram-Hansen](http://www.robots.ox.ac.uk/~bradley) -> Intelligent Networks (Startup)
- [Yuan Zhou](https://yuaanzhou.github.io/) -> Oxford PhD
- [Max Igl](http://maximilianigl.github.io/) -> Waymo
- [Rob Zinkov](http://www.zinkov.com/) -> Oxford PhD
- [Tuan Anh Le](http://www.tuananhle.co.uk/) -> Google Brain
- [Rob Cornish](http://www.robots.ox.ac.uk/~rcornish/index.html) -> Oxford PhD
- [Stefan Webb](http://stefanwebb.uk) -> Twitter
- [Gunes Baydin](http://www.robots.ox.ac.uk/~gunes/) -> Oxford Departmental Lecturer
- [Tobias Kohn](https://www.hughes.cam.ac.uk/about-us/our-people/senior-members/tobias-kohn/) -> Cambridge PostDoc
- [Marcin Szymczak](https://www.inf.ed.ac.uk/people/students/Marcin_Szymczak.html) -> Comarch
- [Tom Rainforth](http://www.robots.ox.ac.uk/~twgr) -> Oxford PostDoc
- [Brooks Paige](http://www.robots.ox.ac.uk/~brooks) -> Unversity College London, Associate Professor
- [Jan Willem van de Meent](http://www.robots.ox.ac.uk/~jwvdm/) -> University of Amsterdam
- [David Tolpin](http://offtopia.net/) -> Ben-Gurion University of the Negev, Assistant Professor
- [Jonathan Huggins](http://www.jhhuggins.org/) -> Boston University, Assistant Professor
- [Willie Neiswanger](http://www.cs.cmu.edu/~wdn/) -> Stanford, Postdoc
- [Carl Smith](https://www.linkedin.com/in/carl-smith-b9823a16/?originalSubdomain=ch) -> Oracle
- [David Pfau](http://davidpfau.com/) -> DeepMind
- [Jan Gasthaus](http://www.gatsby.ucl.ac.uk/~ucabjga/) -> Amazon
- Nicholas Bartlett -> [Nicholas Campelia](https://www.linkedin.com/in/nicholas-campelia-4695a29) -> Two Sigma
- [Yura Perov](https://www.linkedin.com/in/yuraperov/?originalSubdomain=uk) -> Health Tech Startup
- [Tom Jin](https://www.stats.ox.ac.uk/~sjin/) -> Merrill Lynch
- [Neil Dhir](http://www.robots.ox.ac.uk/~neild/)

### Collaborators

- [Mark Schmidt](https://www.cs.ubc.ca/~schmidtm/)
- [Hongseok Yang](https://sites.google.com/view/hongseokyang/home)
- [Yee Whye Teh](http://www.stats.ox.ac.uk/~teh/)
- [Arnaud Doucet](http://www.stats.ox.ac.uk/~doucet/)
- [Vikash Mansingkha](http://web.mit.edu/vkm/www/)
- [Andy Gordon](https://onedrive.live.com/view.aspx/adg?cid=c6149b019d236bf5)
- [Jeffrey Siskind](https://engineering.purdue.edu/~qobi/)
